module.exports = {
  pluginOptions: {
    apollo: {
      enableMocks: true,
      enableEngine: true,
      lintGQL: false
    }
  },
  chainWebpack: config => {
    config.performance.maxEntrypointSize(400000).maxAssetSize(400000);
  }
};
