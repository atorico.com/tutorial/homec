import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import { createProvider } from "./vue-apollo";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.config.productionTip = false;

new Vue({
  store,
  apolloProvider: createProvider(),
  render: h => h(App)
}).$mount("#app");
